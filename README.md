# Readme for final round
# Running trained models on imges and videos
1. clone this repo 
2. change input and output directories properly in general_configs.py
3. change model checkpoint path in mask_rcnn_demo.py
3. run using python mask_rcnn_demo.py

# Finetuning Mask_Rcnn 
# Requirements
* numpy
* scipy
* Pillow
* cython
* matplotlib
* scikit-image
* tensorflow>=1.3.0
* keras>=2.0.8
* opencv-python
* h5py

# Fine tuening mask_rcnn for selected categories
* Please clone mask_rcnn modified repository from https://github.com/ParthaEth/Mask_RCNN.git
* checkout fine_tune branch
* Download mscoco dataset
* Provide modelcheckpoint path in line 2370 in file model.py
* Run using python train --dataset=/home/presentation/dataset/mscoco --model=/home/presentation/resources/mask_rcnn_coco.h5


# Installation using conda
conda create -n <myenv> -f mask_rcnn_env.txt

# ReadMe for round 1
# Requirements
* python 2.7
* Tensorflow 1.12 or greater
* Keras 2.2 or greater
* scipy
* numpy
* matplotlib

# Installation with Conda
conda create -n cacy3 python=3.6 scipy gensim keras tensorflow-gpu pillow

# Training run

Make sure the computer you are running the code has internet access as it would autometically download pretrained imagenet weights

## Step 1.
Download and place the train dataset in a suitable place and create a directory named `logs` in the root directory i.e. inside `MPI_TUE`

## Step 2.
Change the paths corresponding to the training images' directory as well as the `.CSV` file containing training labels in the file `trani_test_script.py` at line numbers `34` and `35` 

## Step3.
The script `trani_test_script.py` takes one integer argument which specifies the run configuration. The run configurations are listed in `config.py` file found in the `config` module
 
 * This script can be run in parallel with multiple different configuration ids to parallely train multiple models
 * Example `python trani_test_script.py 19`

## Step 4.
Visualize in tensorboard and choose best models
 
# Test set solution generation
## Step 1.
Download and place the test dataset in a suitable place

## Step 2.
Change the paths corresponding to the training images' directory in the file `generate_test_set_sub_file.py` at line number `33`

## Step 3.
Choose the best models based on validation score of the training run. Or else you may contact us for trained model weights used for the competition

## Step 4.
Provide paths to the chosen models in `generate_test_set_sub_file.py` at line number `39`

## Step 5.
Run `generate_test_set_sub_file.py` and it will generate the labels in required format when it finishes with the file name `test_pred.csv`. This file can be uploaded directly

* Example `python generate_test_set_sub_file.py`
