# this file contains all the configurations in the form of a list

configurations = \
{0: [{'model_name': "resnet50",
      'input_shape': (224, 224, 3),
      'batch_size': 100,
      'epochs': 700,
      'lr': 1e-3,
      'l2_weight': 0,
      'log_root': './logs'},
                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 5},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 10},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 15},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 20},
                             ],

1: [{'model_name': "VGG16",
     'input_shape': (224, 224, 3),
     'batch_size': 100,
     'epochs': 700,
     'lr': 1e-8,
     'l2_weight': 0,
     'log_root': './logs'},
                            {'expt_name': 'demography_classification',
                             'pooling': 'max',
                             'freeze_n_imagenet_layers': -1},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': -1},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 0},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 5},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 7},
                            ],

2: [{'model_name': "VGG19",
     'input_shape': (224, 224, 3),
     'batch_size': 100,
     'epochs': 700,
     'lr': 1e-8,
     'l2_weight': 0,
     'log_root': './logs'},
                            {'expt_name': 'demography_classification',
                             'pooling': 'max',
                             'freeze_n_imagenet_layers': -1},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': -1},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 0},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 7},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 11},
                            ],

3: [{'model_name': "INCEPTIONV3",
     'input_shape': (299, 299, 3),
     'batch_size': 100,
     'epochs': 700,
     'lr': 1e-3,
     'l2_weight': 0,
     'log_root': './logs'},
                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 5},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 10},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 15},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 20},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 25},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 30},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 40},
                            ],

4: [{'model_name': "INCEPTIONRESNETV2",
     'input_shape': (299, 299, 3),
     'batch_size': 100,
     'epochs': 700,
     'lr': 1e-3,
     'l2_weight': 0,
     'log_root': './logs'},
                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 5},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 10},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 15},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 20},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 25},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 30},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 40},
                            ],

5: [{'model_name': "DENSENET121",
     'input_shape': (224, 224, 3),
     'batch_size': 100,
     'epochs': 700,
     'lr': 1e-3,
     'l2_weight': 0,
     'log_root': './logs'},
                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 5},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 10},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 15},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 20},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 80},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 100},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 110},
                            ],

6: [{'model_name': "DENSENET169",
     'input_shape': (224, 224, 3),
     'batch_size': 100,
     'epochs': 700,
     'lr': 1e-3,
     'l2_weight': 0,
     'log_root': './logs'},
                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 10},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 20},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 40},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 60},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 80},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 100},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 120},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 140},
                            ],

7: [{'model_name': "DENSENET201",
     'input_shape': (224, 224, 3),
     'batch_size': 100,
     'epochs': 700,
     'lr': 1e-3,
     'l2_weight': 0,
     'log_root': './logs'},
                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 10},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 15},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 20},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 60},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 80},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 100},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 120},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 140},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 160},

                            {'expt_name': 'demography_classification',
                             'pooling': 'avg',
                             'freeze_n_imagenet_layers': 180},

                            ],
}