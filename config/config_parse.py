def get_config_idxs(process_idx, config_dict):
    maj_cfg_idx = 0
    count = 0
    while(True):
        num_minor_configs = len(config_dict[maj_cfg_idx])-1
        if count + num_minor_configs >= process_idx + 1:
            minor_cfg_idx = process_idx - count
            break
        else:
            count += num_minor_configs
            maj_cfg_idx += 1
    return maj_cfg_idx, minor_cfg_idx + 1