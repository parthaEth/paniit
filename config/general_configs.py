images_file_path = '/home/presentation/dataset/paniit/training/training/'
csv_file_path = '/home/presentation/dataset/paniit/training/solution.csv'
resource_dir = '/home/presentation/resources'
MSK_RCNN_PATH = '/home/presentation/repos/Mask_RCNN'
COCO_API_DIR = '/home/presentation/repos/cocoapi/PythonAPI'
import sys
sys.path.append(COCO_API_DIR)
FRAMES_SAVE_DIR = "/home/presentation/repos/paniit/mask_rcnn/output_dir/team"
RUN_ON_IMAGES = True
IMAGE_DIR = "/home/presentation/repos/paniit/example_images"

coc_class_names = ['BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
                   'bus', 'train', 'truck', 'boat', 'traffic light',
                   'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
                   'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
                   'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
                   'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
                   'kite', 'baseball bat', 'baseball glove', 'skateboard',
                   'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
                   'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
                   'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
                   'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
                   'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
                   'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
                   'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
                   'teddy bear', 'hair drier', 'toothbrush']

coc_class_names_ours = ['BG', 'person', 'bicycle', 'car', 'motorcycle',
                        'boat', 'cow', 'elephant']
