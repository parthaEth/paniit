import numpy as np
from numpy import genfromtxt
from keras.utils.np_utils import to_categorical
from scipy.misc import imread, imresize
import matplotlib.pyplot as plt
from scipy import ndimage
from glob import glob
import os


def dataloader(image_folder, csv_path, input_size, debug=False):
    filepath = csv_path
    image_folder = image_folder
    my_data = genfromtxt(filepath ,delimiter=',', skip_header=9)
    total_images = np.zeros([my_data.shape[0],input_size, input_size, 3])
    labels = np.zeros([my_data.shape[0], 6])
    for i in range(my_data.shape[0]):
        # print i
        img_name = image_folder + str(int(my_data[i,0])) + '.png'
        try:
            img = imread(img_name)
        except:
            img = plt.imread(img_name)
        # img = ndimage.median_filter(img, 5)
        # img = ndimage.median_filter(img, 5)
        # img = ndimage.median_filter(img, 5)
        total_images[i] = imresize(img, [input_size, input_size], interp='bicubic')
        if debug:
            plt.imshow(total_images[i].astype('uint8'))
            print(img.shape)
            plt.show()
        int_labels = my_data[i,1] - 1
        labels[i] = to_categorical(int(int_labels), num_classes=6)
    np.random.seed(5)
    indices = np.array(range(my_data.shape[0]))
    np.random.shuffle(indices)
    total_images = total_images[indices]
    total_images /= 127.5
    total_images -= 1.0
    labels = labels[indices]
    train_images = total_images[:4096]
    train_labels = labels[:4096]
    val_images = total_images[4096:]
    val_labels = labels[4096:]
    return (train_images, train_labels), (val_images,val_labels)


def add_random_noise(image):
    return image + np.random.uniform(low=-0.01, high=0.01, size=image.shape)


def get_test_yield_iters():
    return (40000/20)
    # return (5000 / 20)


def dataloader_test(image_folder, input_size, debug=False):
    def test_gen():
        total_img_paths = sorted(glob(image_folder + '*.png'), key=str.lower)#sorted(os.listdir(image_folder), key=len) #
        total_images = np.zeros([20, input_size, input_size, 3])
        count = 1
        while count<len(total_img_paths):
            for i in range(20):
                img_name = image_folder + str(count) + '.png'
                try:
                    img = imread(img_name)
                except:
                    img = plt.imread(img_name)
                # img = ndimage.median_filter(img, 5)
                # img = ndimage.median_filter(img, 5)
                # img = ndimage.median_filter(img, 5)
                total_images[i] = imresize(img, [input_size, input_size], interp='bicubic')
                if debug:
                    print(img_name)
                    plt.imshow(total_images[i].astype('uint8'))
                    print(img.shape)
                    plt.show()
                count += 1
            total_images /= 127.5
            total_images -= 1.0
            print(count)
            yield total_images
    return test_gen


if __name__ == '__main__':
    csv_path = 'solution.csv'
    image_folder = '/Users/ssanyal/Downloads/training/training/'
    input_size = 224
    # dataloader(image_folder, csv_path, input_size)
    dataloader_test(image_folder, input_size, debug=False)

