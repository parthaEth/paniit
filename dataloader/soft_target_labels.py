import numpy as np


def get_soft_target_datagen_from_datagen(datagen):
    def _soft_label_gen():
        while True:
            x, y = datagen.next()
            yield x, y + np.random.uniform(-0.1, 0.1, y.shape)
    return _soft_label_gen