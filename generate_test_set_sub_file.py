from dataloader import dataloader
import sys
import os
import errno
from models import get_model
from config import config_parse
from config import config
import numpy as np

if len(sys.argv) > 2:
    raise ValueError("This script takes only one argument whic is experement number.")


def get_model_ensmbl(list_config_ids, list_paths):
    models = []
    test_data_gens = []
    for config_id in range(len(list_config_ids)):
        maj_cfg_idx, minor_cfg_idx = config_parse.get_config_idxs(int(list_config_ids[config_id]),
                                                                  config.configurations)

        # Build model
        nn_model = get_model.get_neural_net_model(
            model_name=config.configurations[maj_cfg_idx][0]['model_name'],
            pooling=config.configurations[maj_cfg_idx][minor_cfg_idx]['pooling'],
            learning_rate=1e-3,
            freeze_n_layers=config.configurations[maj_cfg_idx][minor_cfg_idx]['freeze_n_imagenet_layers'],
            l2_reg_weight=0)

        nn_model.load_weights(list_paths[config_id])
        models.append(nn_model)

        # Test Data Generator
        test_dat_gen = dataloader.dataloader_test('/is/ps2/pghosh/datasets/paniit/testing/',
                                                  input_size=config.configurations[maj_cfg_idx][0]['input_shape'][0])
        test_data_gens.append(test_dat_gen())
    return models, test_data_gens


list_model_path = ['/media/pghosh/Data/repos/paniit/best_models/INCEPTIONV3.h5_best',
                   '/media/pghosh/Data/repos/paniit/best_models/INCEPTIONRESNETV2.h5_best',]
list_config_ids = [15, 24]

models, test_data_gens = get_model_ensmbl(list_config_ids, list_model_path)


# Generate labels
labels = np.zeros((40000, 6))

for mdl_count in range(len(models)):
    for i in range(dataloader.get_test_yield_iters()):
        labels[i*20:i*20+20, :] += models[mdl_count].predict(test_data_gens[mdl_count].next())  # avg, might change later

labels = np.argmax(labels, axis=-1) + 1

id_cat = np.zeros((labels.shape[0], 2))
id_cat[:, 0] = np.array(range(1, 1 + labels.shape[0]))
id_cat[:, 1] = labels

np.savetxt('test_pred.csv', id_cat, delimiter=',', header='id,category', comments='', fmt='%d')
