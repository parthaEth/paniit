
import os, glob
from homogenus.data.body_cropper_I import crop_humans
from homogenus.tools.image_tools import read_prep_image
from homogenus.tools.image_tools import save_images


def paniit19_classify_genders(im_fnames, openpose_path, dataset_name, exp_code, TR = None, accept_threshold = 0.999):
    import tensorflow as tf
    import numpy as np
    import keras
    import cv2
    from homogenus.tools.image_tools import put_text_in_image
    from homogenus.tools.image_tools import fontColors
    from homogenus.data.body_cropper_I import cropout_openpose, should_accept_pose

    from tqdm import tqdm
    import cPickle as pickle
    import json

    from configer import Configer
    import imp

    openpose_gendered_path = openpose_path + '_gendered'
    if not os.path.exists(openpose_gendered_path): os.makedirs(openpose_gendered_path)

    expt_dir = '/genderclassifier/homogenus/results'

    crop_margin = 0.08

    expr_path = os.path.join(expt_dir, exp_code)
    results_dir = os.path.join(expr_path, 'evaluations', 'paniit19_[%.3f]'%accept_threshold, dataset_name)
    if not os.path.exists(results_dir): os.makedirs(results_dir)
    print('Writing results to %s'%results_dir)

    model_dir = os.path.join(expr_path, 'models')
    if TR is None:
        trained_model_fname = sorted(glob.glob(os.path.join(model_dir, '*.ckpt.index')), key=os.path.getmtime)[-1]
    else:
        trained_model_fname = sorted(glob.glob(os.path.join(model_dir, 'TR%s_*.ckpt.index'%TR)), key=os.path.getmtime)[-1]
    trained_model_fname = trained_model_fname.replace('.index', '')
    print('Using Trained Model: %s'%trained_model_fname)

    try_num = os.path.basename(trained_model_fname).split('_')[0]

    config_fname = os.path.join(expr_path, '%s_hg_settings.ini' % try_num)
    hg_ps = Configer(default_ps_fname=config_fname)

    hg_tf_path = os.path.join(expr_path, 'hg_train.py')

    hg_net = lambda Iin: imp.load_source('hg_train', hg_tf_path).hg_net(Iin, is_training=False, want_keras_resnetmodel=True)

    batch_size = 1
    with tf.device("/device:GPU:0"):
        Iph = tf.placeholder(tf.float32, [batch_size, 224, 224, 3])

        logits_out_op, keras_resnetmodel = hg_net(Iph)
        probs_op = tf.nn.softmax(logits_out_op)

    hg_vars = tf.contrib.framework.get_variables('resnet18') + tf.contrib.framework.get_variables('hg_fc')
    saver = tf.train.Saver(var_list=hg_vars, max_to_keep=1000)

    init_zeroout_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())

    with tf.Session() as sess:
        sess.run(init_zeroout_op)
        keras.backend.set_session(sess)
        keras_resnetmodel.load_weights(hg_ps.keras_resnetmodel, by_name = True)
        saver.restore(sess, trained_model_fname)

        for im_fname in tqdm(im_fnames):
            im_basename = os.path.basename(im_fname)
            img_ext = im_basename.split('.')[-1]
            openpose_gendered_fname = os.path.join(openpose_gendered_path, im_basename.replace('.%s'%img_ext, '_keypoints.json'))
            openpose_fname = os.path.join(openpose_path, im_basename.replace('.%s'%img_ext, '_keypoints.json'))
            if os.path.exists(openpose_gendered_fname):
                with open(openpose_gendered_fname) as f: pose_data = json.load(f)
            else:
                with open(openpose_fname) as f: pose_data = json.load(f)

            im_orig = cv2.imread(im_fname, 3)[:,:,::-1].copy()
            for opnpose_pIdx in range(len(pose_data['people'])):
                pose_data['people'][opnpose_pIdx]['gender_pd'] = 'neutral'
                ##### pose_data['people'][opnpose_pIdx]['gender_gt'] = 'female'

                pose = np.asarray(pose_data['people'][opnpose_pIdx]['pose_keypoints_2d']).reshape(-1, 3)
                if not should_accept_pose(pose, human_prob_thr=0.49): continue

                crop_info = cropout_openpose(im_fname, pose, want_image=True, crop_margin=crop_margin)
                cropped_image = crop_info['cropped_image']
                if cropped_image.shape[0] < 200 or cropped_image.shape[1] < 200: continue

                img = read_prep_image(cropped_image)[np.newaxis]

                probs_ob = sess.run(probs_op, feed_dict={Iph: img})[0]
                gender_id = np.argmax(probs_ob, axis=0)
                gender_prob = probs_ob[gender_id]
                if gender_prob>accept_threshold:
                    gender_pd = 'male' if gender_id == 0 else 'female'
                    color = 'green'
                else:
                    gender_pd = 'neutral'
                    gender_id = 2
                    color = 'grey'

                if 'gender_gt' in pose_data['people'][opnpose_pIdx].keys():
                    gender_gt = pose_data['people'][opnpose_pIdx]['gender_gt']
                    if gender_pd == 'neutral' or gender_gt == 'neutral': color = 'grey'
                    elif gender_pd == gender_gt: color = 'green'
                    else:color = 'red'
                    text = 'pred:%s_gt:%s[%.3f]' % (gender_pd, gender_gt, gender_prob)

                else:
                    gender_gt = 'male' if gender_id == 0 else 'female'
                    text = 'thr:%s_pred:%s[%.3f]'%(gender_pd,gender_gt, gender_prob)

                x1 = crop_info['crop_boundary']['offset_width']
                y1 = crop_info['crop_boundary']['offset_height']
                x2 = crop_info['crop_boundary']['target_width'] + x1
                y2 = crop_info['crop_boundary']['target_height'] + y1
                im_orig = cv2.rectangle(im_orig, (x1, y1), (x2, y2), fontColors[color], 2)
                im_orig = put_text_in_image(im_orig, [text], color, (x1, y1))[0]

                pose_data['people'][opnpose_pIdx]['gender_pd'] = gender_pd

            save_images(im_orig, results_dir, [os.path.basename(im_fname)])
            out_json_path = os.path.join(openpose_gendered_path, os.path.basename(openpose_fname))
            with open(out_json_path, 'w') as f: json.dump(pose_data, f)

        n_total = 0
        n_correct = 0
        n_incorrect = 0
        n_discarded = 0
        for im_fname in tqdm(im_fnames):
            im_basename = os.path.basename(im_fname)
            openpose_gendered_fname = os.path.join(openpose_gendered_path, im_basename.replace('.jpg', '_keypoints.json'))
            with open(openpose_gendered_fname) as f: pose_data = json.load(f)

            for opnpose_pIdx in range(len(pose_data['people'])):
                if 'gender_gt' in pose_data['people'][opnpose_pIdx].keys():
                    n_total += 1
                    if pose_data['people'][opnpose_pIdx]['gender_pd'] == 'neutral': n_discarded += 1
                    elif pose_data['people'][opnpose_pIdx]['gender_pd'] == pose_data['people'][opnpose_pIdx]['gender_gt']: n_correct += 1
                    elif pose_data['people'][opnpose_pIdx]['gender_pd'] != pose_data['people'][opnpose_pIdx]['gender_gt']: n_incorrect += 1

        results = {'n_total':n_total,
                   'n_correct':n_correct,
                   'n_incorrect':n_incorrect,
                   'n_discarded':n_discarded,}

        with open(os.path.join(results_dir,'results.json'),'w') as f:
            json.dump(results, f)

        print(results)

def correct_imagenames(im_fnames, im_txt):
    import shutil
    new_im_fnames = []
    for im_fname in im_fnames:
        im_basename = os.path.basename(im_fname)
        _, im_num = im_basename.split('_')
        im_num, im_ext = im_num.split('.')

        im_basename = '%s_%012d.%s'%(im_txt, int(im_num)-1, im_ext)
        new_im_fname = os.path.join(os.path.dirname(im_fname), im_basename)
        im_fname = os.path.join(os.path.dirname(im_fname), im_fname)
        shutil.move(im_fname, new_im_fname)
        new_im_fnames.append(new_im_fname)
    return new_im_fnames

expr_code = '20_T08_V04'
TR = None  # '01'
accept_threshold = 0.9

# dataset_name = 'v7'
# im_fnames = glob.glob('/home/nghorbani/Dropbox/paniit2019/data/sample_vids_images/%s/*.jpg'%dataset_name)
# openpose_path = '/home/nghorbani/Dropbox/paniit2019/data/keypoints/%s'%dataset_name
# #im_fnames = correct_imagenames(im_fnames, dataset_name)
# paniit19_classify_genders(im_fnames, openpose_path, dataset_name, expr_code, TR, accept_threshold)

dataset_name = 'images'
im_fnames = glob.glob('/home/nghorbani/Dropbox/paniit2019/data/images/*.jpg')
openpose_path = '/home/nghorbani/Dropbox/paniit2019/data/keypoints/images'
#im_fnames = correct_imagenames(im_fnames, dataset_name)
paniit19_classify_genders(im_fnames, openpose_path, dataset_name, expr_code, TR, accept_threshold)
