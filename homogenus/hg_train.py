# -*- coding: utf-8 -*-

'''
Homogenus: A human gender classifier
Author: Nima Ghorbani
'''

import os, sys, shutil

import tensorflow as tf
import numpy as np

from datetime import datetime

import tensorflow.contrib.slim as slim
import keras

def hg_net(Iin, is_training=True, reg_coef=1e-3, reuse=tf.AUTO_REUSE, want_keras_resnetmodel = False):

    from homogenus.classification_models.resnet.models import ResNet18

    with tf.name_scope('resnet18'):
        resnet_model = ResNet18(input_tensor=Iin, input_shape=(224, 224, 3), weights=None, classes=None, include_top=False)
        net = resnet_model.get_output_at(-1)

    with tf.variable_scope('hg_fc', reuse=reuse):
        net = keras.layers.Dense(128, name='fc1')(net)
        logits = keras.layers.Dense(2, name='fc2')(net)

    if want_keras_resnetmodel:
        return logits, resnet_model
    else:
        return logits