import cv2


def get_web_cam_feed_generator(file_name, display=False):
    if file_name is not None:
        video_capture = cv2.VideoCapture(file_name)
    else:
        video_capture = cv2.VideoCapture(-1)
    try:
        while True:
            # Capture frame-by-frame
            ret, frame = video_capture.read()
            frame = frame[:, :, ::-1]
            # frame = cv2.resize(frame, (int(frame.shape[1]/4), int(frame.shape[0]/4)))
            if display and frame is not None:
                cv2.imshow('Video', frame)
            if frame is None:
                print('no frame accessed')
            yield frame
            cv2.waitKey(1)

    finally:
        video_capture.release()
        cv2.destroyAllWindows()


if __name__ == "__main__":
    file_name = None
    # file_name = "/home/presentation/dataset/paniit/msk_rcnn_videdo/road_video.mp4"
    frame_gen = get_web_cam_feed_generator(file_name, True)
    while True:
        next(frame_gen)
