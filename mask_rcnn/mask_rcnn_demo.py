import os
import sys
sys.path.append("../")
import matplotlib.pyplot as plt
from config import general_configs
from mask_rcnn import msk_rcnn_configuration
from mask_rcnn.cam_cap import capture_from_webcam
import time
import skimage
import queue
import threading


def viz_loop(input_image_q, outout_anno_q, plt):
    image = input_image_q.get()
    input_image_q.put(image)
    disp_frame = plt.imshow(image[0])
    ax = plt.gca()
    count = 0
    while True:
        # if general_configs.FRAMES_SAVE_DIR is not None:
        #     save_file = os.path.join(general_configs.FRAMES_SAVE_DIR, str(count) + ".jpg")
        image = input_image_q.get()
        input_image_q.put(image)

        for i in range(config.BATCH_SIZE):
            start_time1 = time.time()

            # Visualize results
            r = outout_anno_q.get(block=True, timeout=None)[i]
            visualize.display_instances(image[i], r['rois'], r['masks'], r['class_ids'],
                                        general_configs.coc_class_names, r['scores'],
                                        disp_frame=disp_frame, ax=ax, save_file=None)
            # disp_frame.set_data(image)
            plt.draw()
            plt.pause(0.001)

            count += 1


def input_gather_loop(input_image_q, image):

    while True:
        for i in range(config.BATCH_SIZE):
            image[i] = next(frame_generator)
            input_image_q.put(image)

# Root directory of the project
ROOT_DIR = os.path.abspath("../")
RESOURCE_DIR = general_configs.resource_dir

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn import utils
import mrcnn.model as modellib
from mask_rcnn import visualize

#%matplotlib inline

# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

# Local path to trained weights file
COCO_MODEL_PATH = os.path.join(RESOURCE_DIR, "mask_rcnn_coco.h5")
# COCO_MODEL_PATH = "/home/presentation/repos/paniit/mask_rcnn/saved_models/mask_rcnn_finetuned_01.h5"
# Download COCO trained weights from Releases if needed
if not os.path.exists(COCO_MODEL_PATH):
    utils.download_trained_weights(COCO_MODEL_PATH)

# Directory of images to run detection on
IMAGE_DIR = os.path.join(ROOT_DIR, "example_images")

# Create model object in inference mode.
config = msk_rcnn_configuration.InferenceConfig()
model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)

# Load weights trained on MS-COCO
model.load_weights(COCO_MODEL_PATH, by_name=True)

if general_configs.RUN_ON_IMAGES:
    # Load a random image from the images folder
    # file_names = next(os.walk(IMAGE_DIR))[2]
    # image = skimage.io.imread(os.path.join(IMAGE_DIR, random.choice(file_names)))
    def dir_iterator():
        files = os.listdir(general_configs.IMAGE_DIR)
        for img_file in files:
            yield skimage.io.imread(os.path.join(general_configs.IMAGE_DIR, img_file))


    frame_generator = dir_iterator()

else:
    # Run detection
    frame_generator = capture_from_webcam.get_web_cam_feed_generator(None)
    # frame_generator = capture_from_webcam.get_web_cam_feed_generator("/is/ps2/pghosh/Downloads/DONT CELEBRATE TOO EARLY - SPORT FAILS.mp4")
    # frame_generator = capture_from_webcam.get_web_cam_feed_generator("/home/presentation/dataset/paniit/msk_rcnn_videdo/sample_vids/v.mp4")
    #frame_generator = capture_from_webcam.get_web_cam_feed_generator("/home/presentation/dataset/paniit/msk_rcnn_videdo/sample_vids/v1.mp4")
    #frame_generator = capture_from_webcam.get_web_cam_feed_generator("/home/presentation/dataset/paniit/msk_rcnn_videdo/sample_vids/v2.mp4")
    #frame_generator = capture_from_webcam.get_web_cam_feed_generator("/home/presentation/dataset/paniit/msk_rcnn_videdo/sample_vids/v3.mp4")
    #frame_generator = capture_from_webcam.get_web_cam_feed_generator("/home/presentation/dataset/paniit/msk_rcnn_videdo/sample_vids/v4.mp4")
    # frame_generator = capture_from_webcam.get_web_cam_feed_generator("/home/presentation/dataset/paniit/msk_rcnn_videdo/sample_vids/v5.mp4")
    #frame_generator = capture_from_webcam.get_web_cam_feed_generator("/home/presentation/dataset/paniit/msk_rcnn_videdo/sample_vids/v6.mp4")
    #frame_generator = capture_from_webcam.get_web_cam_feed_generator("/home/presentation/dataset/paniit/msk_rcnn_videdo/sample_vids/v7.mp4")



image = []

for i in range(config.BATCH_SIZE):
    image.append(next(frame_generator))


input_image_q = queue.Queue(3)
outout_anno_q = queue.Queue(3)

input_image_q.put(image)

viz_thread = threading.Thread(target=viz_loop, args=(input_image_q, outout_anno_q, plt))
viz_thread.setDaemon(True)

input_gen_thread = threading.Thread(target=input_gather_loop, args=(input_image_q, image))
input_gen_thread.setDaemon(True)


viz_thread.start()
input_gen_thread.start()


while(True):
    start_time = time.time()
    outout_anno_q.put(model.detect(input_image_q.get(block=True, timeout=None), verbose=1),
                      block=True, timeout=None)
    detection_time = time.time()
    # plt.draw()
    # plt.pause(0.001)

