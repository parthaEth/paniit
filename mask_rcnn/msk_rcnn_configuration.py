import sys
import os
sys.path.append("../")
from config import general_configs
# Import COCO config
sys.path.append(os.path.join(general_configs.MSK_RCNN_PATH, "samples/coco/"))  # To find local version
import coco


class InferenceConfig(coco.CocoConfig):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    BATCH_SIZE = 1


if __name__ == "__main__":
    config = InferenceConfig()
    config.display()
