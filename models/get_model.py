import keras
from keras.optimizers import Adam
from keras.layers import Dense, AveragePooling2D, MaxPooling2D, Dropout
from keras.models import Model
from keras.regularizers import l2


def get_neural_net_model(model_name, pooling, learning_rate, freeze_n_layers, l2_reg_weight):
    if model_name.upper() == 'RESNET50':
        return get_resnet_50(pooling, learning_rate, freeze_n_layers, l2_reg_weight)
    elif model_name.upper() == 'XCEPTION':
        return get_xception(pooling, learning_rate, freeze_n_layers, l2_reg_weight)
    elif model_name.upper() == 'VGG16':
        return get_vgg16(pooling, learning_rate, freeze_n_layers, l2_reg_weight)
    elif model_name.upper() == 'VGG19':
        return get_vgg19(pooling, learning_rate, freeze_n_layers, l2_reg_weight)
    elif model_name.upper() == 'INCEPTIONRESNETV2':
        return get_inception_resnet_v2(pooling, learning_rate, freeze_n_layers, l2_reg_weight)
    elif model_name.upper() == 'DENSENET121':
        return get_DenseNet121(pooling, learning_rate, freeze_n_layers, l2_reg_weight)
    elif model_name.upper() == 'DENSENET169':
        return get_DenseNet169(pooling, learning_rate, freeze_n_layers, l2_reg_weight)
    elif model_name.upper() == 'DENSENET201':
        return get_DenseNet201(pooling, learning_rate, freeze_n_layers, l2_reg_weight)
    elif model_name.upper() == 'INCEPTIONV3':
        return get_InceptionV3(pooling, learning_rate, freeze_n_layers, l2_reg_weight)
    else:
        raise NotImplementedError("Modelname :" + str(model_name) + " Not understood")


def freeze_n_layers_of_mdl(model, n):
    for i in range(len(model.layers)):
        if i >= n and n >= 0:
            break
        model.layers[i].trainable = False
    return model


def apply_l2_regularization(model, l2_weight):
    for i in range(len(model.layers)):
        model.layers[i].kernel_regularizer = l2(l2_weight)
    return model


def get_resnet_50(pooling, learning_rate, freeze_n_layers, l2_reg_weight):
    model = keras.applications.resnet50.ResNet50(include_top=False, weights='imagenet', input_tensor=None,
                                                 input_shape=None, pooling=pooling, classes=6)
    model = freeze_n_layers_of_mdl(model, freeze_n_layers)
    model = apply_l2_regularization(model, l2_reg_weight)
    model = add_dense_soft_max_layer(model)
    model.compile(optimizer=Adam(lr=learning_rate), loss='categorical_crossentropy', metrics=['acc'])
    return model


def get_xception(pooling, learning_rate, freeze_n_layers, l2_reg_weight):
    model = keras.applications.xception.Xception(include_top=False, weights='imagenet', input_tensor=None,
                                                 input_shape=None, pooling=pooling, classes=6)
    model = freeze_n_layers_of_mdl(model, freeze_n_layers)
    model = add_dense_soft_max_layer(model)
    model.compile(optimizer=Adam(lr=learning_rate), loss='categorical_crossentropy', metrics=['acc'])
    return model


def get_vgg16(pooling, learning_rate, freeze_n_layers, l2_reg_weight):
    model = keras.applications.vgg16.VGG16(include_top=False, weights='imagenet', input_tensor=None, input_shape=None,
                                           pooling=pooling, classes=6)
    model = freeze_n_layers_of_mdl(model, freeze_n_layers)
    model = add_dense_soft_max_layer(model, vgg=True)
    model.compile(optimizer=Adam(lr=learning_rate), loss='categorical_crossentropy', metrics=['acc'])
    return model


def get_vgg19(pooling, learning_rate, freeze_n_layers, l2_reg_weight):
    model = keras.applications.vgg19.VGG19(include_top=False, weights='imagenet', input_tensor=None, input_shape=None,
                                           pooling=pooling, classes=6)
    model = freeze_n_layers_of_mdl(model, freeze_n_layers)
    model = add_dense_soft_max_layer(model, vgg=True)
    model.compile(optimizer=Adam(lr=learning_rate), loss='categorical_crossentropy', metrics=['acc'])
    return model


def get_inception_resnet_v2(pooling, learning_rate, freeze_n_layers, l2_reg_weight):
    model = keras.applications.inception_resnet_v2.InceptionResNetV2(include_top=False, weights='imagenet',
                                                                     input_tensor=None, input_shape=None,
                                                                     pooling=pooling, classes=6)
    model = freeze_n_layers_of_mdl(model, freeze_n_layers)
    model = add_dense_soft_max_layer(model)
    model.compile(optimizer=Adam(lr=learning_rate), loss='categorical_crossentropy', metrics=['acc'])
    return model


def get_DenseNet121(pooling, learning_rate, freeze_n_layers, l2_reg_weight):
    model = keras.applications.densenet.DenseNet121(include_top=False, weights='imagenet', input_tensor=None,
                                                    input_shape=None, pooling=pooling, classes=6)
    model = freeze_n_layers_of_mdl(model, freeze_n_layers)
    model = add_dense_soft_max_layer(model)
    model.compile(optimizer=Adam(lr=learning_rate), loss='categorical_crossentropy', metrics=['acc'])
    return model


def get_DenseNet169(pooling, learning_rate, freeze_n_layers, l2_reg_weight):
    model = keras.applications.densenet.DenseNet169(include_top=False, weights='imagenet', input_tensor=None,
                                                    input_shape=None, pooling=pooling, classes=6)
    model = freeze_n_layers_of_mdl(model, freeze_n_layers)
    model = add_dense_soft_max_layer(model)
    model.compile(optimizer=Adam(lr=learning_rate), loss='categorical_crossentropy', metrics=['acc'])
    return model


def get_DenseNet201(pooling, learning_rate, freeze_n_layers, l2_reg_weight):
    model = keras.applications.densenet.DenseNet201(include_top=False, weights='imagenet', input_tensor=None,
                                                    input_shape=None, pooling=pooling, classes=6)
    model = freeze_n_layers_of_mdl(model, freeze_n_layers)
    model = add_dense_soft_max_layer(model)
    model.compile(optimizer=Adam(lr=learning_rate), loss='categorical_crossentropy', metrics=['acc'])
    return model


def get_InceptionV3(pooling, learning_rate, freeze_n_layers, l2_reg_weight):
    model = keras.applications.inception_v3.InceptionV3(include_top=False, weights='imagenet', input_tensor=None,
                                                        input_shape=None, pooling=pooling, classes=6)
    model = freeze_n_layers_of_mdl(model, freeze_n_layers)
    model = add_dense_soft_max_layer(model)
    model.compile(optimizer=Adam(lr=learning_rate), loss='categorical_crossentropy', metrics=['acc'])
    return model


def add_dense_soft_max_layer(model, vgg=False):
    x = model.outputs[0]
    if vgg:
        x = Dense(4096, activation='relu')(x)
        x = Dropout(0.5)(x)
        x = Dense(4096, activation='relu')(x)
        x = Dropout(0.5)(x)
    x = Dense(6, activation='softmax', name='fc6')(x)
    return Model(inputs=model.inputs, outputs=x)