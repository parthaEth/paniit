from dataloader import dataloader
import sys
import os
import errno
from models import get_model
from config import config_parse
from config import config
import keras
from dataloader import soft_target_labels
from config import general_configs

if len(sys.argv) > 2:
    raise ValueError("This script takes only one argument whic is experement number.")

# prepare logging
maj_cfg_idx, minor_cfg_idx = config_parse.get_config_idxs(int(sys.argv[1]), config.configurations)
log_root = config.configurations[maj_cfg_idx][0]['log_root']
log_root = os.path.join(log_root, str(maj_cfg_idx))
try:
    os.makedirs(log_root)
except OSError as e:
    if e.errno != errno.EEXIST:
        raise
log_dir = os.path.join(log_root, config.configurations[maj_cfg_idx][minor_cfg_idx]['expt_name'] +\
                       '_' + str(minor_cfg_idx))

tb_log_dir = os.path.join(log_dir, 'tb')
os.mkdir(log_dir)
os.mkdir(tb_log_dir)

model_name = config.configurations[maj_cfg_idx][0]['model_name'] + '.h5'
model_path = os.path.join(log_dir, model_name)

# Prepare dataloader
(x_tr, y_tr), validation_data = dataloader.dataloader(
    image_folder=general_configs.images_file_path, csv_path=general_configs.csv_file_path,
    input_size=config.configurations[maj_cfg_idx][0]['input_shape'][0])

datagen = keras.preprocessing.image.ImageDataGenerator(
    featurewise_center=True,
    featurewise_std_normalization=True,
    rotation_range=5,
    width_shift_range=0.1,
    height_shift_range=0.1,
    horizontal_flip=False,
    vertical_flip=False,
    shear_range=0.01,
    zoom_range=0.01,
    preprocessing_function=dataloader.add_random_noise)
datagen.fit(x_tr)
train_genertor = soft_target_labels.get_soft_target_datagen_from_datagen(datagen.flow(x=x_tr, y=y_tr, batch_size=32))()

# Build model
nn_model = get_model.get_neural_net_model(
    model_name=config.configurations[maj_cfg_idx][0]['model_name'],
    pooling=config.configurations[maj_cfg_idx][minor_cfg_idx]['pooling'],
    learning_rate=config.configurations[maj_cfg_idx][0]['lr'],
    freeze_n_layers=config.configurations[maj_cfg_idx][minor_cfg_idx]['freeze_n_imagenet_layers'],
    l2_reg_weight=config.configurations[maj_cfg_idx][0]['l2_weight'])
nn_model.summary()

# Callbacks
callbacks = []
red_on_plateau = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=5)
callbacks.append(red_on_plateau)

tb_call_back = keras.callbacks.TensorBoard(log_dir=tb_log_dir, histogram_freq=0, write_graph=False)
callbacks.append(tb_call_back)

mdl_chk_pt = keras.callbacks.ModelCheckpoint(os.path.join(log_dir, model_name)+'_best', monitor='val_loss', verbose=0,
                                             save_best_only=True, save_weights_only=True, mode='auto', period=1)
callbacks.append(mdl_chk_pt)



# Training loop
try:
    nn_model.fit_generator(train_genertor,
                           epochs=config.configurations[maj_cfg_idx][0]['epochs'],
                           callbacks=callbacks, validation_data=validation_data, shuffle=True,
                           steps_per_epoch=x_tr.shape[0]/32+1)
finally:
    nn_model.save_weights(os.path.join(log_dir, model_name))

