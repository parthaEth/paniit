from numpy import genfromtxt
import numpy as np


true_lbl = genfromtxt('/is/ps2/pghosh/datasets/paniit/training/solution.csv' ,delimiter=',', skip_header=1)
gen_lbl = genfromtxt('/media/pghosh/Data/repos/paniit/test_pred.csv' ,delimiter=',', skip_header=1)

matches_percent = np.sum(true_lbl[:, 1] == gen_lbl[:, 1])/5000.0

print(matches_percent)