from dataloader import dataloader


# images_file_path = '/is/ps2/pghosh/datasets/paniit/training/training/'
# csv_file_path = '/is/ps2/pghosh/datasets/paniit/training/solution.csv'
# (x_tr, y_tr), validation_data = dataloader.dataloader(
#     image_folder=images_file_path, csv_path=csv_file_path,
#     input_size=224, debug=True)

test_dat_gen = dataloader.dataloader_test('/is/ps2/pghosh/datasets/paniit/training/training/',
                                          input_size=224, debug=True)

generator = test_dat_gen()
generator.next()
generator.next()